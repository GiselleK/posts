package Beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.ArrayList;

import Modelo.Post;
import Controle.PostControle;

@ManagedBean(name="postBean")
@RequestScoped

public class PostBean {
	ArrayList<Post> lista = new PostControle().consultarTodos();
	private int id;
	private String titulo;
	private String post;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	
	public boolean adicionarPost() {
		boolean resultado = false;
		Post mod = new Post();
		PostControle con = new PostControle();
		try {
			mod.setTitulo(titulo);
			mod.setPost(post);
			con.inserir(mod);
			resultado = true;
			
		}catch(Exception e) {
			e.getMessage();
			
		}
		return resultado;
		
	}
	public ArrayList<Post> getLista() {
		return lista;
	}
	public void setLista(ArrayList<Post> lista) {
		this.lista = lista;
	}
	public Post retornarItem(int id) {
		return lista.get(id);
	}
	
	public String editar() {
		Post mod = new Post();
		PostControle con = new PostControle();
		mod.setId(id);
		mod.setTitulo(titulo);
		mod.setPost(post);
		if(con.atualizar(mod)) {
			return "mostrarPost";	
		}else {
			return "editar";
		}
	}

	public String deletar(int id) {
		new PostControle().deletar(id);
		return "mostrarPost";
	}

	public void carregarId(int id) {
		Post con = new PostControle().consultar(id);
		this.setId(con.getId());
		this.setTitulo(con.getTitulo());
		this.setPost(con.getPost());
	}



}
