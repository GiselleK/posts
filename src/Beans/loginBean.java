package Beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


import Controle.UsuarioControle;
import Modelo.Usuario;

@ManagedBean(name="loginBean")
@SessionScoped

public class loginBean {
	private String nome;
	private String senha;
	private String email;
	
	public String login(){
		String a = "";
		try {
			Usuario usuario  = new Usuario();
			UsuarioControle con = new UsuarioControle();
			usuario.setEmail(email);
			usuario.setSenha(senha);
			if(email != null && senha != null) {
				con.login(usuario);
			}else{				
				
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}

		return a;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
