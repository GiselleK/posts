package Beans;

import java.security.SecureRandom;
import java.util.Base64;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.ArrayList;


import Controle.UsuarioControle;
import Modelo.Usuario;
import Controle.Criptografia;

@ManagedBean(name="userBean")
@RequestScoped

public class UserBean {
	ArrayList<Usuario> lista = new UsuarioControle().consultarTodos();
	private int id;
	private String nome;
	private String senha;
	private String email;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
	public boolean Cadastro(){
		boolean resultado = false;
		UsuarioControle con = new UsuarioControle();
		Usuario user = new Usuario();
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
			keyGenerator.init(256);
			SecretKey key = keyGenerator.generateKey();
			byte[] IV = new byte[16];
			SecureRandom random = new SecureRandom();
			random.nextBytes(IV);
			byte[] CriptografiaEmail = Criptografia.encrypt(email.getBytes(),key, IV);
	        String cpE = Base64.getEncoder().encodeToString(CriptografiaEmail);
	        byte[] CriptografiaSenha = Criptografia.encrypt(senha.getBytes(),key, IV);
	        String cpS = Base64.getEncoder().encodeToString(CriptografiaSenha);
	        String iv = Base64.getEncoder().encodeToString(IV);
	        String keey = Base64.getEncoder().encodeToString(key.getEncoded());
			user.setNome(nome);
			user.setSenha(cpS);
			user.setEmail(cpE);
			user.setKeey(keey);
			user.setIv(iv);
			con.inserir(user);
			resultado = true;
		}catch(Exception e) {
			e.getMessage();
		}			
		return resultado;
	}
	public boolean decriptografar() {
		boolean resultado = false;
		Usuario user = new Usuario();
		try {					
	        byte[] tt = Base64.getDecoder().decode(user.getEmail());
	        byte[] t1 = Base64.getDecoder().decode(user.getSenha());
	        byte[] key = Base64.getDecoder().decode(user.getKeey());
	        byte[] iv = Base64.getDecoder().decode(user.getIv());
	        SecretKey originalKey = new SecretKeySpec(key, 0, key.length, "AES");
	        System.out.println("teste"+tt+"-"+t1);
			String decryptedText = Criptografia.decrypt(tt,originalKey, iv);
	        String t = Criptografia.decrypt(t1,originalKey, iv);
	        System.out.println("DeCrypted Text : "+decryptedText);
	        System.out.println("DeCrypted Text : "+t);
			if(decryptedText.equals(email) && t.equals(senha)){
              resultado = true;			
			}else {
			  resultado = false;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}				
		return resultado;
		
	}
	
	public ArrayList<Usuario> getLista() {
		return lista;
	}
	public void setLista(ArrayList<Usuario> lista) {
		this.lista = lista;
	}
	public Usuario retornarItem(int id) {
		return lista.get(id);
	}
	
	public String editar() {
		Usuario mod = new Usuario();
		UsuarioControle con = new UsuarioControle();
		mod.setId(id);
		mod.setNome(nome);
		mod.setEmail(email);
		mod.setSenha(senha);
		if(con.atualizar(mod)) {
			return "mostrarUser";	
		}else {
			return "editar";
		}
	}

	public String deletar(int id) {
		new UsuarioControle().deletar(id);
		return "mostrarUser";
	}

	public void carregarId(int id) {
		Usuario con = new UsuarioControle().consultar(id);
		this.setId(con.getId());
		this.setNome(con.getNome());
		this.setEmail(con.getEmail());
		this.setSenha(con.getSenha());
	}

	
}

