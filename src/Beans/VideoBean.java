package Beans;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

import Controle.ImagemControle;
import Controle.VideoControle;
import Modelo.Imagem;
import Modelo.Video;

@ManagedBean(name="videoBean")
@RequestScoped


public class VideoBean {
	ArrayList<Video> lista = new VideoControle().consultarTodos();
	private int id;
	private String titulo;
	private Part video;
	private String video2;
	private String men;
	private String pasta = FacesContext.getCurrentInstance().getExternalContext().getRealPath("")+"Video/";
	
	public String getVideo2() {
		return video2;
	}
	public void setVideo2(String video2) {
		this.video2 = video2;
	}
	public String getMen() {
		return men;
	}
	public void setMen(String men) {
		this.men = men;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Part getVideo() {
		return video;
	}
	public void setVideo(Part video) {
		this.video = video;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public String adicionarVideo() throws IOException {
		String resultado = "Deu certo";
		Video vi = new Video();
		VideoControle con = new VideoControle();
		try {
			if(video != null) {
				InputStream vid = video.getInputStream();
				String nome = video.getSubmittedFileName();
				Files.copy(vid, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
				String diretVi = "Video/"+""+nome;
				vi.setVideo(diretVi);
			}else {
				vi.setVideo("");
			}
			vi.setTitulo(titulo);
			con.inserir(vi);
		}catch(Exception e) {
			e.getMessage();	
		}
		return resultado;
	}
	
	public ArrayList<Video> getLista() {
		return lista;
	}
	public void setLista(ArrayList<Video> lista) {
		this.lista = lista;
	}
	public Video retornarItem(int id) {
		return lista.get(id);
	}
	
	public String editar() {
		Video vi = new Video();
		VideoControle con = new VideoControle();
		try {
			if(video != null) {
				InputStream vid = video.getInputStream();
				String nome = video.getSubmittedFileName();
				Files.copy(vid, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
				String diretVi = "Video/"+""+nome;
				vi.setVideo(diretVi);
			}else {
				vi.setVideo(this.getVideo2());
			}
			vi.setTitulo(this.getTitulo());
			vi.setId(this.getId());
			con.atualizar(vi);
		}catch(Exception e) {
			e.getMessage();	
		}
		this.setMen("<script>window.location.href='mostrarVideo.xhtml'</script>");
		return "mostrarVideo";
	}

	public void deletar(int id) {
		new VideoControle().deletar(id);
		this.setMen("<script>window.location.href='mostrarVideo.xhtml'</script>");
	}

	public void carregarId(int id) {
		Video con = new VideoControle().consultar(id);
		this.setId(con.getId());
		this.setTitulo(con.getTitulo());
		this.setVideo2(con.getVideo());
	}
}
