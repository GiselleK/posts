package Beans;

import java.util.ArrayList;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javax.servlet.http.Part;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import Modelo.Imagem;
import Controle.ImagemControle;

@ManagedBean(name="imagemBean")
@RequestScoped

public class ImagemBean {
	ArrayList<Imagem> lista = new ImagemControle().consultarTodos();
	private int id;
	private String titulo;
	private Part imagem;
	private String men;

	private String image;
	private String pasta = FacesContext.getCurrentInstance().getExternalContext().getRealPath("")+"Imagem/";
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getId() {
		return id;
	}
	public String getMen() {
		return men;
	}
	public void setMen(String men) {
		this.men = men;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Part getImagem() {
		return imagem;
	}
	public void setImagem(Part imagem) {
		this.imagem = imagem;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String adicionarImagem() throws IOException {
		String resultado = "Deu certo";
		Imagem im = new Imagem();
		ImagemControle con = new ImagemControle();
		try {
			if(imagem != null) {
				InputStream img = imagem.getInputStream();
				String nome = imagem.getSubmittedFileName();
				Files.copy(img, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
				String diretImg = "Imagem/"+nome;
				im.setImagem(diretImg);
			}else {
				im.setImagem("");
			}
			im.setTitulo(titulo);
			con.inserir(im);
		}catch(Exception e) {
			e.getMessage();	
		}
		return resultado;
	}
	
	public ArrayList<Imagem> getLista() {
		return lista;
	}
	public void setLista(ArrayList<Imagem> lista) {
		this.lista = lista;
	}
	public Imagem retornarItem(int id) {
		return lista.get(id);
	}
	
	public String editar() {
		Imagem im = new Imagem();
		ImagemControle con = new ImagemControle();
		try {
			if(imagem != null) {
				InputStream img = imagem.getInputStream();
				String nome = imagem.getSubmittedFileName();
				Files.copy(img, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
				String diretImg = "Imagem/"+nome;
				im.setImagem(diretImg);
			}else {
				im.setImagem(this.getImage());
			}
			im.setTitulo(this.getTitulo());
			im.setId(this.getId());
			con.atualizar(im);
		}catch(Exception e) {
			System.out.println(e.getMessage());	
		}
		this.setMen("<script>window.location.href='mostrarImagem.xhtml'</script>");
		return "mostrarImagem";
	}

	public void deletar(int id) {
		new ImagemControle().deletar(id);
		this.setMen("<script>window.location.href='mostrarImagem.xhtml'</script>");
	}

	public void carregarId(int id) {
		Imagem con = new ImagemControle().consultar(id);
		this.setTitulo(con.getTitulo());
		this.setImage(con.getImagem());
		this.setId(con.getId());
	}

}
