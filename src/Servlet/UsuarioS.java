package Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;

import Controle.UsuarioControle;
import Modelo.Usuario;
@WebServlet("/UsuarioS")
public class UsuarioS extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			UsuarioControle con = new UsuarioControle();
			Usuario user = new Usuario();
			user.setNome(request.getParameter("nome"));
			user.setSenha(request.getParameter("senha"));
			user.setEmail(request.getParameter("email"));
			con.inserir(user);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}	
		}

}
