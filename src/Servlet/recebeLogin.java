package Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//import javax.servlet.http.Cookie;
import Modelo.Usuario;
import Controle.UsuarioControle;

@WebServlet("/recebeLogin")
public class recebeLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Usuario usuario  = new Usuario();
		UsuarioControle con = new UsuarioControle();
		try {
//			Usuario usuario  = new Usuario();
//			UsuarioControle con = new UsuarioControle();
			String email = request.getParameter("email");
			String senha = request.getParameter("senha");
			usuario.setEmail(email);
			usuario.setSenha(senha);
			Usuario login = con.login(usuario);
			if(login!=null) {
				//Usuario login = con.login(usuario);
				session.setAttribute("login", login);
				response.sendRedirect("cadastro.jsp");
				
			}else{				
				response.sendRedirect("login.jsp");
			}
		}catch(Exception e) {
			e.getMessage();
		}

	}

}
