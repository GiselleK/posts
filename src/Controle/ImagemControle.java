package Controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Modelo.Imagem;
import java.util.ArrayList;

/**
 * @author Mozinho
 *
 */
public class ImagemControle {
	public boolean inserir(Imagem im){
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO Imagem(titulo,imagem) VALUES(?,?);");
			ps.setString(1, im.getTitulo());
			ps.setString(2, im.getImagem());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public boolean atualizar(Imagem im){
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE Imagem SET titulo=?, imagem=? WHERE id=?");
			ps.setString(1, im.getTitulo());
			ps.setString(2, im.getImagem());
			ps.setInt(3, im.getId());
			if(!ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
	public boolean deletar(int id){
		boolean resultado= false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM Imagem WHERE id=?;");
			ps.setInt(1, id);
			if(!ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
	public ArrayList<Imagem> consultarTodos(){
		ArrayList<Imagem> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM imagem ;");
			ResultSet rs = ps.executeQuery();
			if(rs != null){
				lista = new ArrayList<Imagem>();
				while(rs.next()){
					Imagem im = new Imagem();
					im.setTitulo(rs.getString("titulo"));
					im.setImagem(rs.getString("imagem"));
					im.setId(rs.getInt("id"));
					lista.add(im);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;

	}
	public Imagem consultar(int id){
		Imagem im = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Imagem WHERE id=?;");
			ps.setInt(1,id);
			ResultSet rs = ps.executeQuery();
			if(rs != null && rs.next()){
				im = new Imagem();
				im.setId(rs.getInt("id"));
				im.setTitulo(rs.getString("titulo"));
				im.setImagem(rs.getString("imagem"));
				new Conexao().fecharConexao(con);
			}
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return im;

	}



}
