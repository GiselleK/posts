package Controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Modelo.Video;
import java.util.ArrayList;

public class VideoControle {
		public boolean inserir(Video vi){
			boolean resultado = false;
			Connection con = new Conexao().abrirConexao();
			try {
				PreparedStatement ps = con.prepareStatement("INSERT INTO Video(titulo,video) VALUES(?,?);");
				ps.setString(1, vi.getTitulo());
				ps.setString(2, vi.getVideo());
				if(!ps.execute()) {
					resultado = true;
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			return resultado;
		}
		public boolean atualizar(Video vi){
			boolean resultado = false;
			try {
				Connection con = new Conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("UPDATE Video SET titulo=?, video=? WHERE id=?");
				ps.setString(1, vi.getTitulo());
				ps.setString(2, vi.getVideo());
				ps.setInt(3, vi.getId());
				if(!ps.execute()){
					resultado = true;
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
			return resultado;
		}
		public boolean deletar(int id){
			boolean resultado= false;
			try {
				Connection con = new Conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("DELETE FROM Video WHERE id=?;");
				ps.setInt(1, id);
				if(!ps.execute()){
					resultado = true;
					new Conexao().fecharConexao(con);
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
			return resultado;
		}
		public ArrayList<Video> consultarTodos(){
			ArrayList<Video> lista = null;
			try {
				Connection con = new Conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM video ;");
				ResultSet rs = ps.executeQuery();
				if(rs != null){
					lista = new ArrayList<Video>();
					while(rs.next()){
						Video vi = new Video();
						vi.setTitulo(rs.getString("titulo"));
						vi.setVideo(rs.getString("video"));
						vi.setId(rs.getInt("id"));
						lista.add(vi);
					}
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
			return lista;

		}
		public Video consultar(int id){
			Video vi = null;
			try {
				Connection con = new Conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM Video WHERE id=?;");
				ps.setInt(1,id);
				ResultSet rs = ps.executeQuery();
				if(rs != null && rs.next()){
					vi = new Video();
					vi.setId(rs.getInt("id"));
					vi.setTitulo(rs.getString("titulo"));
					vi.setVideo(rs.getString("video"));
					new Conexao().fecharConexao(con);
				}
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
			return vi;

		}
}
