package Controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Modelo.Usuario;
import java.util.ArrayList;

public class UsuarioControle {
	public Usuario login(Usuario user) throws SQLException{
		Usuario useer = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario WHERE nome=? and senha=?");
		ps.setString(1, user.getNome());
		ps.setString(2, user.getSenha());
		ResultSet rs = ps.executeQuery();
		if(rs.next() && rs != null){
			useer = new Usuario();
			//useer.setId(rs.getInt("id"));
			useer.setNome(rs.getString("nome"));
			useer.setSenha(rs.getString("senha"));
		}else{
			useer=null;
		}
		new Conexao().fecharConexao(con);	
		return useer;
	}


	public boolean inserir(Usuario user){
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO Usuario(nome,senha,email,keey,iv) VALUES(?,?,?,?,?);");
			ps.setString(1, user.getNome());
			ps.setString(2, user.getSenha());
			ps.setString(3, user.getEmail());
			ps.setString(4, user.getKeey());
			ps.setString(5, user.getIv());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public boolean atualizar(Usuario user){
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE Usuario SET nome=?, email=?, senha=? WHERE id=?");
			ps.setString(1, user.getNome());
			ps.setString(2, user.getEmail());
			ps.setString(3, user.getSenha());
			ps.setInt(4, user.getId());
			if(!ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
	public boolean deletar(int id){
		boolean resultado= false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM Usuario WHERE id=?;");
			ps.setInt(1, id);
			if(!ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
	public ArrayList<Usuario> consultarTodos(){
		ArrayList<Usuario> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Usuario ;");
			ResultSet rs = ps.executeQuery();
			if(rs != null){
				lista = new ArrayList<Usuario>();
				while(rs.next()){
					Usuario user = new Usuario();
					user.setNome(rs.getString("nome"));
					user.setSenha(rs.getString("senha"));
					user.setEmail(rs.getString("email"));
					user.setId(rs.getInt("id"));
					user.setKeey(rs.getString("keey"));
					user.setIv(rs.getString("iv"));
					lista.add(user);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;

	}

	public Usuario consultar(int id){
		Usuario user = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario WHERE id=?;");
			ps.setInt(1,id);
			ResultSet rs = ps.executeQuery();
			if(rs != null && rs.next()){
				user = new Usuario();
				user.setId(rs.getInt("id"));
				user.setNome(rs.getString("nome"));
				user.setSenha(rs.getString("senha"));
				user.setKeey(rs.getString("keey"));
				user.setIv(rs.getString("iv"));
				new Conexao().fecharConexao(con);
			}
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return user;

	}

}
