package Controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Modelo.Post;
import java.util.ArrayList;

public class PostControle {
	
		public boolean inserir(Post post){
			boolean resultado = false;
			Connection con = new Conexao().abrirConexao();
			try {
				PreparedStatement ps = con.prepareStatement("INSERT INTO Post(titulo,post) VALUES(?,?);");
				ps.setString(1, post.getTitulo());
				ps.setString(2, post.getPost());
				if(!ps.execute()) {
					resultado = true;
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			return resultado;
		}
		public boolean atualizar(Post post){
				boolean resultado = false;
				try {
					Connection con = new Conexao().abrirConexao();
					PreparedStatement ps = con.prepareStatement("UPDATE Post SET titulo=?, post=? WHERE id=?");
					ps.setString(1, post.getTitulo());
					ps.setString(2, post.getPost());
					ps.setInt(3, post.getId());
					if(!ps.execute()){
						resultado = true;
					}
					new Conexao().fecharConexao(con);
				}catch(SQLException e) {
					System.out.println("Erro ao editar: " + e.getMessage());
				}
			return resultado;
		}
		public boolean deletar(int id){
			boolean resultado= false;
			try {
				Connection con = new Conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("DELETE FROM Post WHERE id=?;");
				ps.setInt(1, id);
				if(!ps.execute()){
					resultado = true;
				}					
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro ao deletar: " + e.getMessage());
			}
			return resultado;
		}
		public ArrayList<Post> consultarTodos(){
			ArrayList<Post> lista = null;
			try {
				Connection con = new Conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM Post ;");
				ResultSet rs = ps.executeQuery();
				if(rs != null){
					lista = new ArrayList<Post>();
					while(rs.next()){
						Post post = new Post();
						post.setTitulo(rs.getString("titulo"));
						post.setPost(rs.getString("post"));
						post.setId(rs.getInt("id"));
						lista.add(post);
					}
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
				return lista;

		}

		public Post consultar(int id){
			Post post = null;
			try {
				Connection con = new Conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM Post WHERE id=?;");
				ps.setInt(1,id);
				ResultSet rs = ps.executeQuery();
				if(rs != null && rs.next()){
					post = new Post();
					post.setId(rs.getInt("id"));
					post.setTitulo(rs.getString("titulo"));
					post.setPost(rs.getString("post"));
					new Conexao().fecharConexao(con);
				}
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
					
			return post;

		}
		

	}


